package nf.co.hoko.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.BuildConfig;
import nf.co.hoko.daos.CartItem;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.daos.Product;
import nf.co.hoko.database.helper.OfferHelper;

import static nf.co.hoko.connectivity.ConnectionUtils.*;

/**
 * Created by mlester on 10/21/2015.
 */
public class ConnectionManager {
	private final static String TAG = "HokoNet";

	private static ConnectionManager instance;

	private final Context mContext;

	private ConnectionManager(Context context){mContext = context;}

	public static ConnectionManager getInstance(Context context){
		if(instance == null)
			instance = new ConnectionManager(context);
		return instance;
	}

	public boolean checkConnection(){
		ConnectivityManager cm;
		cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}

	public long authenticateUser(String email, String pass){
		HttpURLConnection con = makeConnection(LOGIN_SERVICE, "email", email, "password", pass);
		try{
			JSONObject result = new JSONObject(readResponse(con));
			return result.getLong("id");
		}
		catch(JSONException | NullPointerException e){
			return -1;
		}
	}

	public List<Offer> getDeals(){
		ArrayList<Offer> offers = new ArrayList<>();
		HttpURLConnection con = makeConnection(OFFERS_SERVICE);
		try{
			JSONArray array = new JSONArray(readResponse(con));
			for(int i = 0; i < array.length(); ++i){
				JSONObject obj = array.getJSONObject(i);
				long product = obj.getLong("product_id");
				String brand = obj.getString("product_brand");
				String name = obj.getString("product_name");
				float price = (float)obj.getDouble("product_price");
				String productImage = IMAGE_PREFIX + obj.getString("image_filename");
				String dateString = obj.getString("date_created");
				String store = obj.getString("store_name");
				String logo = LOGO_PREFIX + obj.getString("store_logo") + ".png";
				long expire = expireOffer(dateString, product);
				Product p = new Product(name, brand, product, productImage);
				offers.add(new Offer(product, p, price, store, logo, expire));
			}
		}
		catch(JSONException | NullPointerException e){
			if(BuildConfig.DEBUG)
				Log.e(TAG, "Error parsing deals array", e);
		}

		return offers;
	}

	public void saveCartItem(CartItem item, long user){
		HttpURLConnection conn = makeConnection(CART_SAVE_SERVICE, "user_id", Long.toString(user),
				"product_id", Long.toString(item.offer.ID),
				"quantity", Integer.toString(item.quantity));
		if(conn != null)
			readResponse(conn);
//			conn.disconnect();
	}

	public void deleteCartItem(CartItem item, long user){
		HttpURLConnection conn = makeConnection(CART_DEL_SERVICE, "user_id", Long.toString(user),
				"product_id", Long.toString(item.offer.ID));
		if(conn != null)
			readResponse(conn);
//			conn.disconnect();
	}

	public List<CartItem> getCart(long user){
		ArrayList<CartItem> cart = new ArrayList<>();
		HttpURLConnection con = makeConnection(CART_GET_SERVICE, "user_id", Long.toString(user));
		if(con != null){
			try{
				JSONArray array = new JSONArray(readResponse(con));
				for(int i = 0; i < array.length(); ++i){
					JSONObject obj = array.getJSONObject(i);
					long offerID = obj.getLong("product_id");
					int quantity = obj.getInt("quantity");
					Offer offer = new OfferHelper(mContext).getOffer(offerID);
					cart.add(new CartItem(offer, quantity));
				}
			}
			catch(JSONException e){
				if(BuildConfig.DEBUG)
					Log.e(TAG, "Error parsing cart array", e);
			}
		}

		return cart;
	}
}
