package nf.co.hoko.connectivity;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import nf.co.hoko.BuildConfig;

/**
 * Created by Ilario Sanseverino on 31/10/15.
 */
public class ConnectionUtils {
	final static String TAG = "HokoNet";
	final static String SERVICE_PREFIX = "http://hoko.co.nf/mobile/";
	final static String LOGIN_SERVICE = SERVICE_PREFIX + "getLogin.php";
	final static String OFFERS_SERVICE = SERVICE_PREFIX + "getOffers.php";
	final static String CART_SAVE_SERVICE = SERVICE_PREFIX + "saveItem.php";
	final static String CART_DEL_SERVICE = SERVICE_PREFIX + "deleteItem.php";
	final static String CART_GET_SERVICE = SERVICE_PREFIX + "getCart.php";

	final static String IMAGE_PREFIX = "http://www.hoko.co.nf/public/img/store_products/";
	static final String LOGO_PREFIX = "http://www.hoko.co.nf/public/img/hoko_tools/";

	static HttpURLConnection makeConnection(String service, String... params){
		boolean doOut = params.length > 0;
		HttpURLConnection con;
		try{
			URL url = new URL(service);
			con = (HttpURLConnection)url.openConnection();
		}
		catch(IOException e){
			if(BuildConfig.DEBUG)
				Log.e(TAG, "IO exception while opening connection", e);
			return null;
		}
		con.setDoInput(true);
		con.setDoOutput(doOut);

		if(doOut){
			try{
				String paramsString = encodeParams(params);
				con.setRequestMethod("POST");
				con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
				con.setRequestProperty( "charset", "utf-8");
				con.setRequestProperty( "Content-Length", Integer.toString(paramsString.length()));
				con.setUseCaches( false );
				Writer os = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
				os.write(paramsString);
				os.flush();
			}
			catch(IOException e){
				if(BuildConfig.DEBUG)
					Log.e(TAG, "IO exception while doing output to server", e);
				return null;
			}
		}
		return con;
	}

	static String readResponse(HttpURLConnection con){
		BufferedReader reader;
		try{
			reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}
		catch(IOException e){
			if(BuildConfig.DEBUG)
				Log.e(TAG, "IO exception while openinng input stream from server", e);
			return null;
		}
		StringBuilder sb = new StringBuilder();
		String line;

		try{
			while((line = reader.readLine()) != null)
				sb.append(line);
			con.disconnect();
			return sb.toString();
		}
		catch(IOException e){
			if(BuildConfig.DEBUG)
				Log.e(TAG, "Error receiving server response", e);
			return null;
		}
	}

	private static String encodeParams(String... params){
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < params.length; ++i){
			try{
				builder.append(URLEncoder.encode(params[i++], "UTF-8")).append("=")
						.append(params[i])
						.append("&");
			}
			catch(UnsupportedEncodingException e){
				if(BuildConfig.DEBUG)
					Log.e(TAG, "Unsupported UTF-8", e);
				return "";
			}
		}
		builder.deleteCharAt(builder.lastIndexOf("&"));
		Log.d(TAG, "Request:\n"+builder);
		return builder.toString();
	}



	static long expireOffer(String createDate, long productID){
		try{
			Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(createDate);
			Calendar cal = Calendar.getInstance();
			Random r = new Random(productID);

//			cal.setTime(date);
			cal.add(Calendar.DAY_OF_YEAR, r.nextInt(15));
			return cal.getTimeInMillis();
		}
		catch(ParseException e){
			return 0l;
		}
	}
}
