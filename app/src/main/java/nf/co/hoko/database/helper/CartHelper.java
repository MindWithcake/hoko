package nf.co.hoko.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.daos.CartItem;
import nf.co.hoko.daos.Offer;

import static android.content.Context.MODE_PRIVATE;
import static nf.co.hoko.activities.main.MainActivity.*;
import static nf.co.hoko.database.contract.CartContract.*;

/**
 * Created by Ilario Sanseverino on 30/10/15.
 */
public class CartHelper extends AbstractDaoHelper<CartItem> {
	public CartHelper(Context context){
		super(context);
	}

	@Override protected ContentValues fromDAO(CartItem dao){
		ContentValues values = new ContentValues();
		values.put(COL_USER, getCurrentUser());
		values.put(COL_OFFER, dao.offer.ID);
		values.put(COL_QUANTITY, dao.quantity);
		return values;
	}

	@Override protected CartItem fromCursor(Cursor cur){
		long offerID = cur.getLong(cur.getColumnIndex(COL_OFFER));
		Offer offer = new OfferHelper(mContext).getOffer(offerID);
		int quantity = cur.getInt(cur.getColumnIndex(COL_QUANTITY));
		return new CartItem(offer, quantity);
	}

	@NonNull @Override protected String getTableName(){
		return TABLE;
	}

	public List<CartItem> getUserCart(long userID){
		String[] columns = {COL_OFFER, COL_QUANTITY};
		String selection = COL_USER + "=?";
		String[] args = {Long.toString(userID)};

		List<CartItem> items = new ArrayList<>();
		SQLiteDatabase db = mHelper.openDatabase();
		Cursor cur = db.query(TABLE, columns, selection, args, null, null, null);
		try{
			while(cur.moveToNext())
				items.add(fromCursor(cur));
			return items;
		}
		finally{
			cur.close();
			mHelper.closeDatabase();
		}
	}

	public void deleteItem(long offerID){
		String selection = COL_USER + "=? AND " + COL_OFFER + "=?";
		String[] args = {Long.toString(getCurrentUser()), Long.toString(offerID)};

		SQLiteDatabase db = mHelper.openDatabase();
		try{
			db.delete(TABLE, selection, args);
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	public void cleanCart(long userID){
		SQLiteDatabase db = mHelper.openDatabase();
		try{
			db.delete(TABLE, COL_USER+"=?", new String[]{Long.toString(userID)});
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	public void addItem(CartItem item){
		ContentValues cv = fromDAO(item);
		SQLiteDatabase db = mHelper.openDatabase();
		try{
			db.insertOrThrow(TABLE, null, cv);
		}
		catch(SQLiteConstraintException e){
			String[] columns = {COL_QUANTITY};
			String selection = COL_USER + "=? AND " + COL_OFFER + "=?";
			String[] args= {Long.toString(getCurrentUser()), Long.toString(item.offer.ID)};
			int quantity = 0;
			Cursor cur = db.query(TABLE, columns, selection, args, null, null, null);
			try{
				if(cur.moveToNext())
					quantity = cur.getInt(cur.getColumnIndex(COL_QUANTITY));
			}
			finally{
				cur.close();
			}
			item.quantity += quantity;
			saveDAO(item);
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	private long getCurrentUser(){
		SharedPreferences pref = mContext.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
		return pref.getLong(USER_KEY, -1);
	}
}
