package nf.co.hoko.database.contract;

import static nf.co.hoko.database.DatabaseHelper.*;

/**
 * Contract for the Cart table. The Cart table contains the items in a shopping list. For every
 * product in the user shop list this table will have an entry with the user ID, the offer ID (to
 * get the product, price and store) and the quantity. Created by Ilario Sanseverino on 29/10/15.
 */
public abstract class CartContract {
	private CartContract(){}

	public static final String TABLE = "tbCart";

	public static final String COL_USER = "cartUser";
	public static final String COL_OFFER = "cartOffer";
	public static final String COL_QUANTITY = "cartQuantity";

	public static final String TYPE_USER = " INTEGER NOT NULL";
	public static final String TYPE_OFFER = " INTEGER NOT NULL";
	public static final String TYPE_QUANTITY = " REAL";

	public static final String CREATE = createCommand(TABLE, COL_QUANTITY + TYPE_QUANTITY,
			COL_OFFER + TYPE_OFFER + foreignKey(OfferContract.TABLE), COL_USER + TYPE_USER,
			"UNIQUE(" + COL_USER + "," + COL_OFFER + ")");
}
