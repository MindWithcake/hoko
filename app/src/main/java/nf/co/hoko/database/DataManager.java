package nf.co.hoko.database;

import android.content.Context;

import java.util.List;

import nf.co.hoko.activities.main.MainActivity;
import nf.co.hoko.connectivity.ConnectionManager;
import nf.co.hoko.daos.CartItem;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.database.helper.CartHelper;
import nf.co.hoko.database.helper.OfferHelper;
import nf.co.hoko.database.helper.ProductHelper;

/**
 * Created by Ilario Sanseverino on 26/10/15.
 */
public class DataManager {
	public static long getUser(Context context){
		return context.getSharedPreferences(MainActivity.PREFERENCES_NAME, Context.MODE_PRIVATE)
				.getLong(MainActivity.USER_KEY, -1);
	}

	public static boolean refreshData(Context context){
		ConnectionManager cm = ConnectionManager.getInstance(context);
		if(!cm.checkConnection())
			return false;

		List<Offer> deals = cm.getDeals();
		ProductHelper productHelper = new ProductHelper(context);
		for(Offer offer: deals)
			productHelper.saveDAO(offer.product);
		new OfferHelper(context).saveList(deals);

		refreshCart(context);

		return true;
	}

	public static void refreshCart(Context context){
		List<CartItem> cart = ConnectionManager.getInstance(context).getCart(getUser(context));
		if(cart != null){
			CartHelper helper = new CartHelper(context);
			helper.cleanCart(getUser(context));
			helper.saveList(cart);
		}
	}

	public static List<Offer> getOffers(Context context){
		OfferHelper helper = new OfferHelper(context);
		return helper.getAllDeals();
	}

	public static List<Offer> getWeeklyDeals(Context context){
		OfferHelper helper = new OfferHelper(context);
		return helper.getWeeklyDeals();
	}

	public static List<CartItem> getShopList(long user, Context context){
		return new CartHelper(context).getUserCart(user);
	}

	public static void addToCart(long user, Offer offer, Context context){
		CartItem toSave = new CartItem(offer, 1);
		new CartHelper(context).addItem(toSave);
		ConnectionManager.getInstance(context).saveCartItem(toSave, user);
	}

	public static void saveCartItem(long user, CartItem item, Context context){
		new CartHelper(context).saveDAO(item);
		ConnectionManager.getInstance(context).saveCartItem(item, user);
	}

	public static void removeCartItem(long user, CartItem item, Context context){
		new CartHelper(context).deleteItem(item.offer.ID);
		ConnectionManager.getInstance(context).deleteCartItem(item, user);
	}
}
