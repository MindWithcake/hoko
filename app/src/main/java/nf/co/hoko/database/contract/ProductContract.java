package nf.co.hoko.database.contract;

import nf.co.hoko.database.DatabaseHelper;

/**
 * Created by Ilario Sanseverino on 29/10/15.
 */
public abstract class ProductContract {
	private ProductContract(){}

	public static final String TABLE = "tbProduct";

	public static final String COL_NAME = "procudtName";
	public static final String COL_BRAND = "productBrand";
	public static final String COL_IMAGE = "productImage";
	public static final String COL_DESCRIPTION = "productDescription";

	public static final String TYPE_NAME = " TEXT NOT NULL";
	public static final String TYPE_BRAND = " TEXT";
	public static final String TYPE_IMAGE = " TEXT";
	public static final String TYPE_DESCR = " TEXT";

	public static final String CREATE = DatabaseHelper.createCommand(TABLE, COL_NAME + TYPE_NAME,
			COL_BRAND + TYPE_BRAND, COL_IMAGE + TYPE_IMAGE, COL_DESCRIPTION + TYPE_DESCR);
}
