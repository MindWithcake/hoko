package nf.co.hoko.database.contract;

import nf.co.hoko.database.DatabaseHelper;

/**
 * Created by Ilario Sanseverino on 29/10/15.
 */
public abstract class OfferContract {
	private OfferContract(){}

	public static final String TABLE = "tbOffer";

	public static final String COL_PRODUCT = "offerProduct";
	public static final String COL_PRICE = "offerPrice";
	public static final String COL_STORE = "offerStoreName";
	public static final String COL_LOGO = "offerStoreLogo";
	public static final String COL_EXPIRE = "offerExpireDate";

	public static final String TYPE_PRODUCT = " INTEGER NOT NULL";
	public static final String TYPE_PRICE = " FLOAT";
	public static final String TYPE_STORE = " TEXT NOT NULL";
	public static final String TYPE_LOGO = " TEXT";
	public static final String TYPE_EXPIRE = " INTEGER";

	public static final String CREATE = DatabaseHelper.createCommand(TABLE, COL_PRICE + TYPE_PRICE,
			COL_PRODUCT + TYPE_PRODUCT + DatabaseHelper.foreignKey(ProductContract.TABLE),
			COL_STORE + TYPE_STORE, COL_LOGO + TYPE_LOGO, COL_EXPIRE + TYPE_EXPIRE);
}
