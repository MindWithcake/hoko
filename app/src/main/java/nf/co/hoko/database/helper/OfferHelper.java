package nf.co.hoko.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nf.co.hoko.daos.Offer;
import nf.co.hoko.daos.Product;

import static nf.co.hoko.database.contract.OfferContract.*;

/**
 * Created by Ilario Sanseverino on 30/10/15.
 */
public class OfferHelper extends AbstractDaoHelper<Offer> {
	public OfferHelper(Context context){
		super(context);
	}

	public Offer getOffer(long id){
		String selection = BaseColumns._ID + "=?";
		String[] args = {Long.toString(id)};
		SQLiteDatabase db = mHelper.openDatabase();
		Cursor cur = db.query(TABLE, null, selection, args, null, null, null);
		try{
			if(cur.moveToNext())
				return fromCursor(cur);
			return null;
		}
		finally{
			cur.close();
			mHelper.closeDatabase();
		}
	}

	public List<Offer> getWeeklyDeals(){
		Calendar cal = Calendar.getInstance();
		long minDate = cal.getTimeInMillis();
		cal.add(Calendar.DATE, 7);
		long maxDate = cal.getTimeInMillis();

		String selection = COL_EXPIRE + " > ? AND " + COL_EXPIRE + " < ?";
		String[] args = {Long.toString(minDate), Long.toString(maxDate)};
		List<Offer> toReturn = new ArrayList<>();

		SQLiteDatabase db = mHelper.openDatabase();
		Cursor cur = db.query(TABLE, null, selection, args, null, null, null);
		try{
			while(cur.moveToNext())
				toReturn.add(fromCursor(cur));
		}
		finally{
			cur.close();
			mHelper.closeDatabase();
		}

		return toReturn;
	}

	public List<Offer> getAllDeals(){
		List<Offer> offers = new ArrayList<>();
		SQLiteDatabase db = mHelper.openDatabase();
		Cursor cur = db.query(TABLE, null, null, null, null, null, null);
		try{
			while(cur.moveToNext())
				offers.add(fromCursor(cur));
			return offers;
		}
		finally{
			cur.close();
			mHelper.closeDatabase();
		}
	}

	@Override protected ContentValues fromDAO(Offer offer){
		ContentValues cv = new ContentValues();
		cv.put(COL_PRODUCT, offer.product.ID);
		cv.put(COL_PRICE, offer.price);
		cv.put(COL_STORE, offer.storeName);
		cv.put(COL_LOGO, offer.storeImage);
		cv.put(COL_EXPIRE, offer.expireDate);
		cv.put(BaseColumns._ID, offer.ID);
		return cv;
	}

	@Override protected Offer fromCursor(Cursor cur){
		long prodID = cur.getLong(cur.getColumnIndex(COL_PRODUCT));
		Product prod = new ProductHelper(mContext).getProduct(prodID);
		long offerID = cur.getLong(cur.getColumnIndex(BaseColumns._ID));
		float price = cur.getFloat(cur.getColumnIndex(COL_PRICE));
		String store = cur.getString(cur.getColumnIndex(COL_STORE));
		String logo = cur.getString(cur.getColumnIndex(COL_LOGO));
		long date = cur.getLong(cur.getColumnIndex(COL_EXPIRE));
		return new Offer(offerID, prod, price, store, logo, date);
	}

	@Override protected String getTableName(){
		return TABLE;
	}
}
