package nf.co.hoko.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.BaseColumns;

import nf.co.hoko.database.contract.CartContract;
import nf.co.hoko.database.contract.OfferContract;
import nf.co.hoko.database.contract.ProductContract;

/**
 * Created by mlester on 10/20/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "db_hoko";
	private static final int DATABASE_VERSION = 2;

	private static final String[] CREATE_QUERY = {ProductContract.CREATE, OfferContract.CREATE,
			CartContract.CREATE};

	private static DatabaseHelper instance;

	private int dbAccessCount = 0;
	private SQLiteDatabase db = null;

	public static DatabaseHelper getInstance(Context context){
		if(instance == null)
			instance = new DatabaseHelper(context);
		return instance;
	}

	private DatabaseHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override public void onCreate(SQLiteDatabase db){
		for(int x = 0; x < CREATE_QUERY.length; x++){
			db.execSQL(CREATE_QUERY[x]);
		}
	}

	@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		if(oldVersion == 1){
			for(String s : new String[]{CartContract.TABLE, OfferContract.TABLE, ProductContract
					.TABLE})
				db.execSQL("DROP TABLE " + s);
			onCreate(db);
		}
	}

	/**
	 * Utility method to make the "CREATE TABLE..." SQL command. The table will have the "_id"
	 * column as primary key. Typical usage is from the Contract classes: {@code public final
	 * static
	 * String CREATE = createCommand(TABLE, COL_1 + " TEXT", COL_2 + " REAL NOT NULL", COL_3 +
	 * "INTEGER REFERENCES " + OtherContract.TABLE + "(" + OtherContract._ID + ")");}
	 *
	 * @param table the name of the table to be created.
	 * @param values a list of columns to add to the table, in the form "NAME TYPE[ CONSTRAINTS]".
	 * @return a String with the SQLITE command to create the desired table.
	 */
	public static String createCommand(String table, String... values){
		StringBuilder builder = new StringBuilder("CREATE TABLE ").append(table).append("(");
		builder.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY");
		for(String value : values)
			builder.append(", ").append(value);
		return builder.append(")").toString();
	}

	/**
	 * Utility method to create a foreign key constraint for the values passed to the {@link
	 * #createCommand(String, String...)} method.
	 *
	 * @param foreignTable the name of the referenced table
	 * @param foreignCol the name of the referenced column
	 * @return a constraint involving a single column with cascade policy for both delete and
	 * update
	 */
	public static String foreignKey(String foreignTable, String foreignCol){
		return " REFERENCES " + foreignTable + "(" + foreignCol + ") ON DELETE NO ACTION" +
				" ON UPDATE CASCADE";
	}

	public static String foreignKey(String foreignTable){
		return foreignKey(foreignTable, BaseColumns._ID);
	}

	/**
	 * Thread safe method to open a database connection.
	 *
	 * @return a writable SQLite database.
	 */
	public synchronized SQLiteDatabase openDatabase(){
		++dbAccessCount;
		if(db == null)
			db = super.getWritableDatabase();
		return db;
	}

	/**
	 * Threadsafe method to close the database. Ensures that no one is using it before performing
	 * the actual closing.
	 */
	public synchronized void closeDatabase(){
		if(--dbAccessCount == 0){
			db.close();
			db = null;
		}
	}

	@Override public SQLiteDatabase getWritableDatabase(){
		return openDatabase();
	}

	@Override public SQLiteDatabase getReadableDatabase(){
		return openDatabase();
	}

	@Override public void onConfigure(SQLiteDatabase db){
		super.onConfigure(db);

		// Enable foreign key constraints
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
			db.setForeignKeyConstraintsEnabled(true);
		else
			db.execSQL("PRAGMA foreign_keys=ON;");
	}
}
