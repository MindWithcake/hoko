package nf.co.hoko.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import nf.co.hoko.daos.Product;

import static nf.co.hoko.database.contract.ProductContract.*;

/**
 * Created by Ilario Sanseverino on 30/10/15.
 */
public class ProductHelper extends AbstractDaoHelper<Product> {
	public ProductHelper(Context context){
		super(context);
	}

	public Product getProduct(long id){
		String[] columns = {BaseColumns._ID, COL_NAME, COL_BRAND, COL_IMAGE};
		String selection = BaseColumns._ID + "=?";
		String[] args = {Long.toString(id)};
		SQLiteDatabase db = mHelper.openDatabase();
		try{
			return fromCursor(db.query(TABLE, columns, selection, args, null, null, null));
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	@Override protected ContentValues fromDAO(Product product){
		ContentValues cv= new ContentValues();
		cv.put(COL_NAME, product.name);
		cv.put(COL_BRAND, product.brand);
		cv.put(COL_IMAGE, product.image);
		cv.putNull(COL_DESCRIPTION);
		cv.put(BaseColumns._ID, product.ID);
		return cv;
	}

	@Override protected Product fromCursor(Cursor cur){
		try{
			cur.moveToNext();
			String name = cur.getString(cur.getColumnIndex(COL_NAME));
			String brand = cur.getString(cur.getColumnIndex(COL_BRAND));
			String image = cur.getString(cur.getColumnIndex(COL_IMAGE));
			long id = cur.getLong(cur.getColumnIndex(BaseColumns._ID));
			return new Product(name, brand, id, image);
		}
		finally{
			cur.close();
		}
	}

	@Override protected String getTableName(){
		return TABLE;
	}
}
