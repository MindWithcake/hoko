package nf.co.hoko.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.List;

import nf.co.hoko.database.DatabaseHelper;

/**
 * Created by Ilario Sanseverino on 30/10/15.
 */
public abstract class AbstractDaoHelper<T> {
	protected final Context mContext;
	protected final DatabaseHelper mHelper;

	public AbstractDaoHelper(Context context){
		mContext = context;
		mHelper = DatabaseHelper.getInstance(context);
	}

	protected abstract ContentValues fromDAO(T dao);
	protected abstract T fromCursor(Cursor cur);
	@NonNull protected abstract String getTableName();

	public void saveList(List<T> list){
		SQLiteDatabase db = mHelper.openDatabase();
		try{
			db.beginTransaction();
			try{
				for(T t: list)
					saveDAO(getTableName(), db, fromDAO(t));
				db.setTransactionSuccessful();
			}
			finally{
				db.endTransaction();
			}
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	public void saveDAO(T dao){
		SQLiteDatabase db = mHelper.openDatabase();
		try{
			saveDAO(getTableName(), db, fromDAO(dao));
		}
		finally{
			mHelper.closeDatabase();
		}
	}

	protected void saveDAO(String table, SQLiteDatabase db, ContentValues cv){
		db.insertWithOnConflict(table, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
	}
}
