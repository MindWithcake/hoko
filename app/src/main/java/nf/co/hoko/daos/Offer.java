package nf.co.hoko.daos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ilario Sanseverino on 23/10/15.
 */
public class Offer implements Parcelable {
	public Product product;
	public float price;
	public String storeName;
	public String storeImage;
	public long expireDate;
	public long ID;

	public Offer(){}

	public Offer(long ID, Product product, float price, String storeName, String storeImage,
			long expireDate){
		this.ID = ID;
		this.product = product;
		this.price = price;
		this.storeName = storeName;
		this.storeImage = storeImage;
		this.expireDate = expireDate;
	}

	protected Offer(Parcel in){
		product = in.readParcelable(Product.class.getClassLoader());
		price = in.readFloat();
		storeName = in.readString();
		storeImage = in.readString();
		expireDate = in.readLong();
		ID = in.readLong();
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeParcelable(product, flags);
		dest.writeFloat(price);
		dest.writeString(storeName);
		dest.writeString(storeImage);
		dest.writeLong(expireDate);
		dest.writeLong(ID);
	}

	@Override public int describeContents(){
		return 0;
	}

	public static final Creator<Offer> CREATOR = new Creator<Offer>() {
		@Override public Offer createFromParcel(Parcel in){
			return new Offer(in);
		}

		@Override public Offer[] newArray(int size){
			return new Offer[size];
		}
	};
}
