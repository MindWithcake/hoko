package nf.co.hoko.daos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ilario Sanseverino on 23/10/15.
 */
public class Product implements Parcelable {
	public String name;
	public String brand;
	public long ID;
	public String image;

	public Product(){}

	public Product(String name, String brand, long ID, String image){
		this.name = name;
		this.brand = brand;
		this.ID = ID;
		this.image = image;
	}

	protected Product(Parcel in){
		name = in.readString();
		brand = in.readString();
		ID = in.readLong();
		image = in.readString();
	}

	public static final Creator<Product> CREATOR = new Creator<Product>() {
		@Override public Product createFromParcel(Parcel in){
			return new Product(in);
		}

		@Override public Product[] newArray(int size){
			return new Product[size];
		}
	};

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){

		dest.writeString(name);
		dest.writeString(brand);
		dest.writeLong(ID);
		dest.writeString(image);
	}
}
