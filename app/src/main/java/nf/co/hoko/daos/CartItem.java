package nf.co.hoko.daos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ilario Sanseverino on 23/10/15.
 */
public class CartItem implements Parcelable {
	public Offer offer;
	public int quantity;

	public CartItem(){}

	public CartItem(Offer offer, int quantity){
		this.offer = offer;
		this.quantity = quantity;
	}

	protected CartItem(Parcel in){
		offer = in.readParcelable(Offer.class.getClassLoader());
		quantity = in.readInt();
	}

	public static final Creator<CartItem> CREATOR = new Creator<CartItem>() {
		@Override public CartItem createFromParcel(Parcel in){
			return new CartItem(in);
		}

		@Override public CartItem[] newArray(int size){
			return new CartItem[size];
		}
	};

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){

		dest.writeParcelable(offer, flags);
		dest.writeInt(quantity);
	}
}
