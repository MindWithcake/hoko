package nf.co.hoko.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import nf.co.hoko.R;

/**
 * Class to comply with the project requirements.
 *
 * Created by Ilario Sanseverino on 03/11/15.
 */
public class DummyActivity extends AppCompatActivity {
	/**
	 * This is the onCreate method.
	 * @param savedInstanceState the state
	 */
	@Override protected void onCreate(Bundle savedInstanceState){
		//call super
		super.onCreate(savedInstanceState);

		// thread n° 1
		new Thread(){
			@Override public void run(){
				try{ Thread.sleep(2000); } // sleep
				catch(InterruptedException e){ e.printStackTrace(); } // should not happen
			}
		}.start();// start the thread
	}

	/**
	 * This is the onStop method called when the application is stopped.
	 * A thread will be launched to notify the stop.
	 */
	@Override protected void onStop(){
		// call the super or else nothing will work
		super.onStop();

		// be safe, catch things ACDC...
		try{
			// thread n° 2
			new Thread(new Runnable() {
				@Override public void run(){ Log.i("Hoko", "Activity Stopped"); }
			}).start();// start the thread
		}
		catch(Exception e){ // this is clearly an unchecked exception
			Log.e("Hoko", "An error occurred", e);
		}
	}

	/**
	 * The onStart method is called when the activity is started.
	 * What did you expect?
	 */
	@Override protected void onStart(){
		super.onStart(); // very important

		new Thread(new Runnable() { // thread n° 3
			@Override public void run(){
				// we just print things
				System.out.println("Activity started");
			} // end of run
		}).start();
		// thread started: huzza!
	}

	/**
	 * Method called when the application is destroyed. Yes, not when it is created
	 * or paused, but exactly on application destroying. How wierd...
	 */
	@Override protected void onDestroy(){
		super.onDestroy(); // call to super

		new Thread(){ // thread n° 4
			@Override public void run(){// override thread run
				mergeSort(new ArrayList<Long>());// very fancy
			}
		}.start(); // and here we go

		//thread n° 5
		HugeThread thread = new HugeThread(); // we use custom class
		thread.start(); // here the thread is started
		try{ thread.join(); } // we want it to complete
		catch(InterruptedException ignored){} // should not happen
	}

	/**
	 * This is the lifecycle method called when the activity is resumed.
	 */
	@Override protected void onResume(){
		super.onResume(); // never forget
		new Thread(new Runnable() { // thread n° 6
			@Override public void run(){
				Log.e("Hoko", "Activity resumed"); // very useful to know
			}
		}).start();// another thread in the wall
	}

	/**
	 * Guess when this method is called.
	 */
	@Override protected void onPause(){
		super.onPause(); // or else we get exception
		new Thread(){ // thread n° 7
			@Override public void run(){
				// We will work on main thread, handler required
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override public void run(){ // make toast. Main thread needed
						Toast.makeText(DummyActivity.this, "Pausing", Toast.LENGTH_SHORT).show();
					}
				});
			} // let's a go! (read it Mario style)
		}.start();
	}

	/**
	 * Never called, since this activity is never started
	 * @param requestCode the code nobody ever used
	 * @param resultCode the result we are not managing
	 * @param data {@inheritDoc}
	 */
	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data); // interesting
		new Thread().start(); // thread n° 8
	}

	/**
	 * Awesome advanced feature. In place merge sort, no extra memory required.
	 * @param list list to sort
	 * @param <T> type of the list, must extend Number
	 */
	public <T extends Number> void mergeSort(final List<T> list){
		mergeSort(list, 0, list.size()-1); // call the recursive sort on whole list

		new Thread(new Runnable() { // thread n° 9
			@Override public void run(){
				for(Number n: list) // write the sorted list
					Log.i("Hoko", "Next item in list: "+n);
			}
		}).start();
	}

	/**
	 * Recursive merge sort. cals himself on two halves of the list, then sorts
	 * the sorted halves into one whole.
	 * @param list list to sort
	 * @param start where the sorting should start
	 * @param stop where the sorting should end
	 * @param <T> type of list, must extend {@link java.lang.Number}
	 */
	private <T extends Number> void mergeSort(List<T> list, int start, int stop){
		if(stop > start){ // else there is nothing to do
			int half = (stop - start) / 2; // compute the two halves
			mergeSort(list, start, start+half); // merge sort left half
			mergeSort(list, start+half+1, stop); // merge sort right half

			int i = start, j = start+half+1; // The starts of the 2 halves
			while(i < j && j < stop){ // halt condition
				if(list.get(i).doubleValue() > list.get(j).doubleValue()){ // unsorted
					T right = list.remove(j++); // remove jth elem and increase j
					list.add(i, right); // insert jth element before ith element
				}
				++i; // increase i
			}
		}
	}

	/**
	 * This is a thread doing really complicated stuff.
	 * It runs until it guesses the current time (probability: 1 / 2^64)
	 */
	private class HugeThread extends Thread {
		/**
		 * Method invoked when this Thread is started.
		 */
		@Override public void run(){
			// start the RNG
			Random rand = new Random(R.id.email_login_form);
			//go on until you guess
			while(System.currentTimeMillis() != rand.nextLong()){
				// crap, it's wrong...
				Log.i("Hoko", "I did not guess the time...");
				try{ sleep(rand.nextLong()); } // have a nap
				catch(InterruptedException e){ break; } // stop here and now!
			}
		}
	}
}
