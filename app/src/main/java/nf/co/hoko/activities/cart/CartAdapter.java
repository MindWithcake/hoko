package nf.co.hoko.activities.cart;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.CartItem;
import nf.co.hoko.database.DataManager;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class CartAdapter extends RecyclerView.Adapter<CartHolder> {
	private List<CartItem> items;

	public CartAdapter(List<CartItem> items){
		this.items = items;
	}

	@Override public CartHolder onCreateViewHolder(ViewGroup parent, int viewType){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new CartHolder(inflater.inflate(R.layout.view_product, parent, false));
	}

	@Override public void onBindViewHolder(CartHolder holder, int position){
		holder.populate(items.get(position));
	}

	@Override public int getItemCount(){
		return items.size();
	}

	public class SwipeDeleteHelper extends SimpleCallback implements OnClickListener {
		private CartItem swiped;
		private int position;

		public SwipeDeleteHelper(){
			super(0, ItemTouchHelper.END);
		}

		@Override
		public boolean onMove(RecyclerView recyclerView, ViewHolder holder, ViewHolder target){
			return false;
		}

		@Override public void onSwiped(ViewHolder holder, int direction){
			final View v = holder.itemView;
			position = holder.getAdapterPosition();
			swiped = items.remove(position);

			notifyItemRemoved(position);
			new Thread(){
				@Override public void run(){
					long user = DataManager.getUser(v.getContext());
					DataManager.removeCartItem(user, swiped, v.getContext()); }
			}.start();

			Snackbar.make(v, R.string.snackbar_removed, Snackbar.LENGTH_SHORT)
					.setAction(R.string.snackbar_undo, this).show();
		}

		@Override public void onClick(final View v){
			items.add(position, swiped);
			notifyItemInserted(position);
			new Thread(new Runnable() {
				@Override public void run(){
					long user = DataManager.getUser(v.getContext());
					DataManager.saveCartItem(user, swiped, v.getContext());
				}
			}).start();
		}
	}
}
