package nf.co.hoko.activities.browse;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;

import static android.support.v7.widget.LinearLayoutManager.*;

/**
 * A simple {@link Fragment} subclass. Use the {@link ProductDetailFragment#newInstance} factory
 * method to create an instance of this fragment.
 */
public class ProductDetailFragment extends Fragment {
	private static final String ARG_LIST = "LIST";

	private List<Offer> mOffers;

	/**
	 * Use this factory method to create a new instance of this fragment using the provided
	 * parameters.
	 *
	 * @param offers list of offers to show.
	 * @return A new instance of fragment ProductDetailFragment.
	 */
	public static ProductDetailFragment newInstance(List<Offer> offers){
		ProductDetailFragment fragment = new ProductDetailFragment();
		Bundle args = new Bundle();
		args.putParcelableArrayList(ARG_LIST, new ArrayList<>(offers));
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		if(getArguments() != null){
			mOffers = getArguments().getParcelableArrayList(ARG_LIST);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		// Inflate the layout for this fragment
		View root = inflater.inflate(R.layout.fragment_product_detail, container, false);
		RecyclerView recyclerView = (RecyclerView)root.findViewById(R.id.product_list);
		LayoutManager manager = new LinearLayoutManager(getActivity(), VERTICAL, false);
		ProductAdapter adapter = new ProductAdapter(mOffers);

		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(manager);
		recyclerView.setAdapter(adapter);

		return root;
	}
}
