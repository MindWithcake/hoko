package nf.co.hoko.activities.deals;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.database.DataManager;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class DealsFragment extends Fragment {
	private ViewPager mPager;
	private View mLeftArrow, mRightArrow;
	private DealsTask task;

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_deals, container, false);

		mPager = (ViewPager)root.findViewById(R.id.deals_pager);
		mLeftArrow = root.findViewById(R.id.left_arrow);
		mRightArrow = root.findViewById(R.id.right_arrow);

		(task = new DealsTask()).execute();

		return root;
	}

	@Override public void onDestroyView(){
		task.cancel(true);
		super.onDestroyView();
	}

	private class DealsTask extends AsyncTask<Void, Void, List<Offer>>{
		@Override protected List<Offer> doInBackground(Void... params){
			return DataManager.getWeeklyDeals(getContext());
		}

		@Override protected void onPostExecute(List<Offer> offers){
			ViewPager.OnPageChangeListener listener = new DealsListener();
			mPager.setAdapter(new DealsPagerAdapter(getChildFragmentManager(), offers));
			mPager.addOnPageChangeListener(listener);
			listener.onPageSelected(0);
		}
	}

	private class DealsListener extends ViewPager.SimpleOnPageChangeListener{
		@Override public void onPageSelected(int pos){
			int maxPos = mPager.getAdapter().getCount() - 1;
			mLeftArrow.setVisibility(pos == 0? View.INVISIBLE : View.VISIBLE);
			mRightArrow.setVisibility(pos == maxPos || pos < 0? View.INVISIBLE : View.VISIBLE);
		}
	}
}
