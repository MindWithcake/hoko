package nf.co.hoko.activities.main;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.Stack;

import nf.co.hoko.R;
import nf.co.hoko.activities.browse.ProductFragment;
import nf.co.hoko.activities.cart.CartFragment;
import nf.co.hoko.activities.deals.DealsFragment;
import nf.co.hoko.activities.login.LoginFragment;

/**
 * Created by Ilario Sanseverino on 03/11/15.
 */
public class DrawerActivity extends AppCompatActivity implements OnNavigationItemSelectedListener {
	protected final static String SEARCHABLE_FRAG = "DrawerActivity.ProductFragment";

	protected Toolbar mToolbar;
	protected Stack<Integer> mIndexes = new Stack<>();
	private DrawerLayout mDrawer;
	private NavigationView mNavView;

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initDrawer();
	}

	@Override public void onBackPressed(){
		DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		if(drawer.isDrawerOpen(GravityCompat.START)){
			drawer.closeDrawer(GravityCompat.START);
		}
		else{
			super.onBackPressed();
			Integer selected = mIndexes.empty()? R.id.nav_products : mIndexes.pop();
			mNavView.setCheckedItem(selected);
		}
	}

	@Override public boolean onNavigationItemSelected(MenuItem item){
		// Handle navigation view item clicks here.
		int id = item.getItemId();
		if(id != mIndexes.peek()) // no change occurred
			showFragment(id);

		return false;
	}

	private void initDrawer(){
		mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		mDrawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
				R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		mDrawer.setDrawerListener(toggle);
		toggle.syncState();

		mNavView = (NavigationView)findViewById(R.id.nav_view);
		mNavView.setNavigationItemSelectedListener(this);
		mNavView.setCheckedItem(R.id.nav_products);
	}

	private void showFragment(Fragment frag, String tag, int title){
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_container, frag, tag)
				.addToBackStack(null)
				.commit();
		mToolbar.setTitle(title);
	}

	private void showFragment(Fragment fragment, int title){
		showFragment(fragment, null, title);
	}

	protected void showFragment(int menuId){
		switch(menuId){
		case R.id.nav_products:
			showFragment(new ProductFragment(), SEARCHABLE_FRAG, R.string.title_browse);
			break;
		case R.id.nav_deals:
			showFragment(new DealsFragment(), R.string.title_deals);
			break;
		case R.id.nav_login:
			showFragment(new LoginFragment(), R.string.title_login);
			break;
		case R.id.nav_cart:
			showFragment(new CartFragment(), R.string.title_cart);
			break;
		}

		mIndexes.push(menuId);
		mNavView.setCheckedItem(menuId);
		mDrawer.closeDrawer(GravityCompat.START);
	}
}
