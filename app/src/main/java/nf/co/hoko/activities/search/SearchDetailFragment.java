package nf.co.hoko.activities.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class SearchDetailFragment extends Fragment {
	private final static String ARG_DEAL = "DealDetailFragment.DEAL";

	private Offer mDeal;

	public static SearchDetailFragment newInstance(Offer deal){
		SearchDetailFragment frag = new SearchDetailFragment();
		Bundle arg = new Bundle();
		arg.putParcelable(ARG_DEAL, deal);
		frag.setArguments(arg);
		return frag;
	}

	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		if(getArguments() != null)
			mDeal = getArguments().getParcelable(ARG_DEAL);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_deal_detail, container, false);

		ImageView logo = (ImageView)root.findViewById(R.id.store_logo);
		ImageView productImage = (ImageView)root.findViewById(R.id.product_image);
		TextView price = (TextView)root.findViewById(R.id.deal_price);
		TextView date = (TextView)root.findViewById(R.id.deal_expire_date);

		if(mDeal.product.image != null && !mDeal.product.image.isEmpty())
			Picasso.with(getContext()).load(mDeal.product.image).into(productImage);
		if(mDeal.storeImage != null && !mDeal.storeImage.isEmpty())
			Picasso.with(getContext()).load(mDeal.storeImage).into(logo);
		price.setText(String.format("%.2f $", mDeal.price));
		date.setText(DateFormat.getDateInstance().format(new Date(mDeal.expireDate)));

		return root;
	}
}
