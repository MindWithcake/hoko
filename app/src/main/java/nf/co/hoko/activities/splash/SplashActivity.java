package nf.co.hoko.activities.splash;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Random;

import nf.co.hoko.R;
import nf.co.hoko.activities.main.MainActivity;
import nf.co.hoko.database.DataManager;

public class SplashActivity extends AppCompatActivity {
	private final static int NUM_TRIES = 5;

	private final Context This = this;

	private View ciaoLogo;
	private View hokoLogo;
	
	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		ciaoLogo = findViewById(R.id.ciao_logo);
		hokoLogo = findViewById(R.id.hoko_logo);
	}

	@Override protected void onStart(){
		super.onStart();
		new LogoTask().execute();
	}

	private class LogoTask extends AsyncTask<Void, Void, Void> {
		@Override protected Void doInBackground(Void... params){
			try{
				Thread.sleep(1000 + new Random().nextInt(500));
			}
			catch(InterruptedException ignored){}

			return null;
		}

		@Override protected void onPostExecute(Void v){
			final int time = getResources().getInteger(android.R.integer.config_mediumAnimTime);
			hokoLogo.setAlpha(0f);
			hokoLogo.setVisibility(View.VISIBLE);
			hokoLogo.animate()
					.alpha(1f)
					.setDuration(time)
					.setListener(new AnimatorListenerAdapter() {
						@Override public void onAnimationStart(Animator animation){
							ciaoLogo.animate()
									.alpha(0f)
									.setDuration(time)
									.setListener(new AnimatorListenerAdapter() {
										@Override public void onAnimationEnd(Animator animation){
											ciaoLogo.setVisibility(View.GONE);
											new DataTask().execute();
										}
									});
						}
					});
		}
	}

	private class DataTask extends AsyncTask<Void, Void, Void> {
		@Override protected Void doInBackground(Void... params){
			/*
			Try to download fresh data from server. Stop when data is saved or too many tries
			in a row have failed.
			 */
			for(int i = 0; !DataManager.refreshData(This) && i < NUM_TRIES; ++i){
				try{ Thread.sleep(1000); } // wait 1 second before trying again
				catch(InterruptedException ignored){}
			}

			return null;
		}

		@Override protected void onPostExecute(Void aVoid){
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
			finish(); // we don't want the splash screen on the back stack
		}
	}
}
