package nf.co.hoko.activities.cart;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import nf.co.hoko.R;
import nf.co.hoko.daos.CartItem;
import nf.co.hoko.daos.Product;
import nf.co.hoko.database.DataManager;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class CartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private final Context mContext;
	private ImageView mProductImage;
	private TextView mProductName;
	private ImageView mStoreLogo;
	private TextView mPrice;
	private CartItem mCartItem;

	public CartHolder(View itemView){
		super(itemView);
		mContext = itemView.getContext();
		mProductImage = (ImageView)itemView.findViewById(R.id.product_image);
		mProductName = (TextView)itemView.findViewById(R.id.product_name);
		mPrice = (TextView)itemView.findViewById(R.id.product_price);
		mStoreLogo = (ImageView)itemView.findViewById(R.id.store_logo);
		FloatingActionButton fab = (FloatingActionButton)itemView.findViewById(R.id.cart_fab);
		fab.setImageResource(R.drawable.fab_plus);
		fab.setOnClickListener(this);
		fab = (FloatingActionButton)itemView.findViewById(R.id.minus_fab);
		fab.setVisibility(View.VISIBLE);
		fab.setOnClickListener(this);
	}

	public void populate(CartItem item){
		mCartItem = item;
		Product product = item.offer.product;
		String storeImage = item.offer.storeImage;
		if(product.image != null && !product.image.isEmpty())
			Picasso.with(mContext).load(product.image).into(mProductImage);
		if(storeImage != null && !storeImage.isEmpty())
			Picasso.with(mContext).load(storeImage).into(mStoreLogo);

		float price = item.offer.price;
		int quantity = item.quantity;
		mPrice.setText(String.format("%d \u00d7 %.2f = %.2f $", quantity, price, quantity * price));
		mProductName.setText(product.name);
	}

	@Override public void onClick(final View v){
		mCartItem.quantity += v.getId() == R.id.cart_fab? 1 : -1;
		new Thread(new Runnable() {
			@Override public void run(){
				DataManager.saveCartItem(DataManager.getUser(v.getContext()), mCartItem, mContext);
			}
		}).start();
		populate(mCartItem);
	}
}
