package nf.co.hoko.activities.deals;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.daos.Offer;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class DealsPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<Offer> mOffers;

	public DealsPagerAdapter(FragmentManager fm, List<Offer> offers){
		super(fm);
		mOffers = new ArrayList<>(offers);
	}

	@Override public Fragment getItem(int position){
		return DealDetailFragment.newInstance(mOffers.get(position));
	}

	@Override public int getCount(){
		return mOffers.size();
	}
}
