package nf.co.hoko.activities.browse;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {
	private final List<Offer> mDataSet;

	public ProductAdapter(List<Offer> data){
		mDataSet = data;
	}

	@Override public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new ProductHolder(inflater.inflate(R.layout.view_product, parent, false));
	}

	@Override public void onBindViewHolder(ProductHolder holder, int position){
		holder.populate(mDataSet.get(position));
	}

	@Override public int getItemCount(){
		return mDataSet.size();
	}
}
