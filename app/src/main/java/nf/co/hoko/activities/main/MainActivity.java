package nf.co.hoko.activities.main;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import nf.co.hoko.R;
import nf.co.hoko.activities.browse.ProductFragment;
import nf.co.hoko.activities.search.SearchActivity;

public class MainActivity extends DrawerActivity {
	public static final String PREFERENCES_NAME = "HokoPreferences";
	public static final String USER_KEY = "UserID";
	private final static String SEARCHABLE_FRAG = "ProductFragment";

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		if(savedInstanceState == null){
			getSupportFragmentManager().beginTransaction()
					.add(R.id.main_container, new ProductFragment(), SEARCHABLE_FRAG)
					.commit();
			mIndexes.push(R.id.nav_products);
		}
	}

	@Override public boolean onCreateOptionsMenu(Menu menu){
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
		ComponentName cn = new ComponentName(this, SearchActivity.class);
		searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item){
		// Handle action bar item clicks here. The action bar will automatically handle clicks on
		// the Home/Up button, so long as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		//noinspection SimplifiableIfStatement
		if(id == R.id.action_search){
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
}
