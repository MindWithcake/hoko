package nf.co.hoko.activities.cart;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.activities.login.LoginFragment;
import nf.co.hoko.daos.CartItem;
import nf.co.hoko.database.DataManager;

import static android.support.design.widget.Snackbar.*;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class CartFragment extends Fragment {
	private RecyclerView mList;
	private CartTask task;

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_cart, container, false);

		mList = (RecyclerView)root.findViewById(R.id.cart_list);
		mList.setLayoutManager(new LinearLayoutManager(mList.getContext()));
		(task = new CartTask()).execute();

		return root;
	}

	@Override public void onDestroyView(){
		task.cancel(true);
		super.onDestroyView();
	}

	private class CartTask extends AsyncTask<Void, Void, List<CartItem>>{
		@Override protected List<CartItem> doInBackground(Void... params){
			long user = DataManager.getUser(getContext());
			if(user > 0)
				return DataManager.getShopList(user, getContext());
			return null;
		}

		@Override protected void onPostExecute(List<CartItem> cartItems){
			if(cartItems == null){
				Snackbar snack = Snackbar.make(mList, R.string.error_must_log, LENGTH_LONG);
				snack.setAction(R.string.snackbar_login, new View.OnClickListener() {
					@Override public void onClick(View v){
						getFragmentManager().beginTransaction()
								.replace(R.id.main_container, new LoginFragment())
								.addToBackStack(null)
								.commit();
					}
				});
				snack.show();
			}
			else{
				CartAdapter adapter = new CartAdapter(cartItems);
				mList.setAdapter(adapter);
				new ItemTouchHelper(adapter.new SwipeDeleteHelper()).attachToRecyclerView(mList);
			}
		}
	}
}
