package nf.co.hoko.activities.browse;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.database.DataManager;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private final Context mContext;
	private ImageView mProductImage;
	private TextView mProductName;
	private ImageView mStoreLogo;
	private TextView mPrice;
	private Offer offer;

	public ProductHolder(View itemView){
		super(itemView);
		mContext = itemView.getContext();
		mProductImage = (ImageView)itemView.findViewById(R.id.product_image);
		mProductName = (TextView)itemView.findViewById(R.id.product_name);
		mPrice = (TextView)itemView.findViewById(R.id.product_price);
		mStoreLogo = (ImageView)itemView.findViewById(R.id.store_logo);
		itemView.findViewById(R.id.cart_fab).setOnClickListener(this);
	}

	public void populate(Offer offer){
		this.offer = offer;
		if(offer.product.image != null && !offer.product.image.isEmpty())
			Picasso.with(mContext).load(offer.product.image).into(mProductImage);
		if(offer.storeImage != null && !offer.storeImage.isEmpty())
			Picasso.with(mContext).load(offer.storeImage).into(mStoreLogo);
		mPrice.setText(String.format("%.2f $", offer.price));
		mProductName.setText(offer.product.name);
	}

	@Override public void onClick(final View v){
		new Thread(new Runnable() {
			@Override public void run(){
				DataManager.addToCart(DataManager.getUser(v.getContext()), offer, v.getContext());
			}
		}).start();
	}
}
