package nf.co.hoko.activities.browse;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.daos.Offer;

/**
 * Created by Ilario Sanseverino on 26/10/15.
 */
public class ProductPagerAdapter  extends FragmentStatePagerAdapter {
	private ArrayList<Offer> mOffers;
	private String mProductFilter = "";

	public ProductPagerAdapter(FragmentManager fm, List<Offer> offers){
		super(fm);
		mOffers = new ArrayList<>(offers);
	}

	@Override public Fragment getItem(int position){
		String filter = Filters.values()[position].filter;
		ArrayList<Offer> filtered = new ArrayList<>();
		for(Offer offer: mOffers){
			if(offer.storeName.matches(filter) && offer.product.name.toLowerCase().contains(
					mProductFilter))
				filtered.add(offer);
		}

		return ProductDetailFragment.newInstance(filtered);
	}

	@Override public int getCount(){
		return Filters.values().length;
	}

	@Override public CharSequence getPageTitle(int position){
		return Filters.values()[position].title;
	}

	@Override public int getItemPosition(Object object){
		return POSITION_NONE;
	}

	public void filterProducts(String filter){
		mProductFilter = filter;
	}

	protected enum Filters{
		NewWorld("New World", "^N.*"),
		CountDown("Countdown", "^C.*"),
		PakNSave("Pak 'n Save", "^P.*"),
		All("All Products", ".*");

		public final String title;
		public final String filter;

		Filters(String title, String filter){
			this.title = title;
			this.filter = filter;
		}
	}
}
