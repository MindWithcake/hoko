package nf.co.hoko.activities.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Iterator;
import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.database.DataManager;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class SearchActivity extends AppCompatActivity {
	private ViewPager mPager;
	private View mLeftArrow, mRightArrow;
	private ProductsTask task;
	private String mQuery = "";

	@Nullable @Override
	public void onCreate(Bundle savedState){
		super.onCreate(savedState);
		setContentView(R.layout.fragment_deals);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		mPager = (ViewPager)findViewById(R.id.deals_pager);
		mLeftArrow = findViewById(R.id.left_arrow);
		mRightArrow = findViewById(R.id.right_arrow);

		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction()))
			mQuery = intent.getStringExtra(SearchManager.QUERY).toLowerCase();

		(task = new ProductsTask()).execute();
	}

	@Override protected void onNewIntent(Intent intent){
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) { // search query changed
			task.cancel(true);
			mQuery = intent.getStringExtra(SearchManager.QUERY);
			(task = new ProductsTask()).execute();
		}

	}

	@Override public void onDestroy(){
		task.cancel(true);
		super.onDestroy();
	}

	private class ProductsTask extends AsyncTask<Void, Void, List<Offer>>{
		@Override protected List<Offer> doInBackground(Void... params){
			List<Offer> offers = DataManager.getOffers(SearchActivity.this);
			Iterator<Offer> it = offers.iterator();
			while(it.hasNext()){
				Offer o = it.next();
				if(!o.product.name.toLowerCase().contains(mQuery))
					it.remove();
			}
			return offers;
		}

		@Override protected void onPostExecute(List<Offer> offers){
			ViewPager.OnPageChangeListener listener = new ProductListener();
			mPager.setAdapter(new SearchPagerAdapter(getSupportFragmentManager(), offers));
			mPager.addOnPageChangeListener(listener);
			listener.onPageSelected(0);
		}
	}

	private class ProductListener extends ViewPager.SimpleOnPageChangeListener{
		@Override public void onPageSelected(int pos){
			int maxPos = mPager.getAdapter().getCount() - 1;
			mLeftArrow.setVisibility(pos == 0? View.INVISIBLE : View.VISIBLE);
			mRightArrow.setVisibility(pos == maxPos || maxPos < 0? View.INVISIBLE : View.VISIBLE);
		}
	}
}
