package nf.co.hoko.activities.search;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import nf.co.hoko.daos.Offer;

/**
 * Created by Ilario Sanseverino on 27/10/15.
 */
public class SearchPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<Offer> mOffers;

	public SearchPagerAdapter(FragmentManager fm, List<Offer> offers){
		super(fm);
		mOffers = new ArrayList<>(offers);
	}

	@Override public Fragment getItem(int position){
		return SearchDetailFragment.newInstance(mOffers.get(position));
	}

	@Override public int getCount(){
		return mOffers.size();
	}
}
