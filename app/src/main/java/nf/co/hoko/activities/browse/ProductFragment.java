package nf.co.hoko.activities.browse;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nf.co.hoko.R;
import nf.co.hoko.daos.Offer;
import nf.co.hoko.database.DataManager;

/**
 * Fragment showing the list of products available.
 */
public class ProductFragment extends Fragment {
	public final static String FILTER_TAG = "nf.co.hoko.activities.browse.ProductFragment.FILTER";

	private ViewPager pager;
	private TabLayout tabs;

	private OfferTask task;
	private ProductPagerAdapter mAdapter;
	private String mFilter;

	public ProductFragment(){
		setArguments(new Bundle());
	}
	
	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mFilter = getArguments().getString(FILTER_TAG, "");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_product, container, false);

		pager = (ViewPager)rootView.findViewById(R.id.products_pager);
		tabs = (TabLayout)rootView.findViewById(R.id.sliding_tabs);
		(task = new OfferTask()).execute();

		return rootView;
	}

	@Override public void onDestroyView(){
		task.cancel(true);
		super.onDestroyView();
	}

	public void filterProducts(String filter){
		mFilter = filter;
		if(mAdapter != null){
			mAdapter.filterProducts(filter);
			mAdapter.notifyDataSetChanged();
			pager.setCurrentItem(ProductPagerAdapter.Filters.All.ordinal());
		}
	}

	private class OfferTask extends AsyncTask<Void, Void, List<Offer>>{
		@Override protected List<Offer> doInBackground(Void... params){
			return DataManager.getOffers(getContext());
		}

		@Override protected void onPostExecute(List<Offer> offers){
			FragmentManager fm = getChildFragmentManager();
			mAdapter = new ProductPagerAdapter(fm, offers);
			mAdapter.filterProducts(mFilter);
			try{
				pager.setAdapter(mAdapter);
				tabs.setupWithViewPager(pager);
			}
			catch(IllegalStateException ignored){}
		}
	}
}
